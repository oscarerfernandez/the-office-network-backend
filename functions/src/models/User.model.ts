import { Id } from 'interfaces';

/**
 * user model proper
 */
export class UserModel implements Id {
  public id?: string;
  public createdAt: string;
  public userId?: string;
  public firstName: string;
  public lastName: string;
  public avatar?: string;
  public email: string;
  public mainLanguage?: string;
  public type?: string;
  public role?: string;
  public name?: string;
  public username: string;
  public provider?: string;
  public description?: string;
  public status?: string;
  public phone?: number;

  /**
   * user model constructor
   */
  constructor(model: UserModel) {
    this.id = model.id;
    this.createdAt = model.createdAt;
    this.userId = model.userId;
    this.firstName = model.firstName;
    this.lastName = model.lastName;
    this.avatar = model.avatar;
    this.email = model.email;
    this.mainLanguage = model.mainLanguage;
    this.type = model.type;
    this.role = model.role;
    this.name = model.name;
    this.username = model.username;
    this.provider = model.provider;
    this.description = model.description;
    this.status = model.status;
    this.phone = model.phone;
  }
}
