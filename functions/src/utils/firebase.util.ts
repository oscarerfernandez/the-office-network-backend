import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
// import { Config } from '../classes';

export const initFirebase = (): void => {
	// settings i.e. firestore its on you
	// init firebase to access its services
	admin.initializeApp(functions.config().firebase);
};
