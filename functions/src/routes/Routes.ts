import { BaseRoutes } from './Base.route'
import { Auth } from '../Middleware/firebaseAuth'
import { UserRoutes } from './User.route'

export class Routes {
  public auth: Auth = new Auth()
  public userRouter: UserRoutes = new UserRoutes()
  public generalRouter: BaseRoutes = new BaseRoutes()

  constructor (api:any) {
    this.routeMiddleware(api)
    this.routerSetup(api)
  }

  public routeMiddleware(api:any) {
    api.use(this.auth.firebaseAuth)
  }

  public routerSetup(api:any):void {
    this.userRouter.routes(api)
    this.generalRouter.routes(api)
  }
}