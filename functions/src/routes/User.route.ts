import * as express from 'express'
import { UserController } from '../controller/Users.controller'

export class UserRoutes {
  public userController: UserController;
  public router: express.Router = express.Router()

  public routes(api: { use: (arg0: string, arg1: express.Router) => void }):any {
    this.router.get('/', this.userController.get)
    this.router.post('/', this.userController.create)
    this.router.get('/:id', this.userController.find)
    this.router.put('/:id', this.userController.update)
    this.router.delete('/:id',this.userController.delete)

    api.use('/users', this.router)
  }
}