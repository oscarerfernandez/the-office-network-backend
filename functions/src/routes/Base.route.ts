import { BaseController } from '../controller/Base.controller';
import * as express from 'express'

export class BaseRoutes {
  public BaseController: BaseController = new BaseController()
  public router: express.Router = express.Router()

  public routes(api:any):any {
    this.router.get('/', this.BaseController.getRoutes)
    this.router.get('/handshake', this.BaseController.handShake)
    this.router.all('*', this.BaseController.notFound)

    api.use('/', this.router)
  }
}