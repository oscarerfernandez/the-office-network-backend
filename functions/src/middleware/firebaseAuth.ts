import * as admin from 'firebase-admin'

export class Auth {

  public firebaseAuth(req: { header: (arg0: string) => string }, res: { locals: { user: admin.auth.DecodedIdToken }; status: (arg0: number) => { (): any; new(): any; json: { (arg0: { error: string }): void; new(): any } } }, next: () => void): void {
    if (req.header('Authorization')) {
      admin.auth().verifyIdToken(req.header('Authorization'))
        .then(user => {
          res.locals.user = user
          next()
        })
        .catch(err => {res.status(401).json(err)})
    } else {
      res.status(401).json({ error: 'Authorization header is not found'})
    }
  }
}
