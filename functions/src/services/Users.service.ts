import {UserModel} from 'models';
import {MyError} from './my-error';
import {BaseService} from './base.service';

export class UserService extends BaseService<UserModel> {
    public isAuthenticated = true;

    constructor(public myErr?: MyError) {
        super(myErr, 'usuarios');
    }
}