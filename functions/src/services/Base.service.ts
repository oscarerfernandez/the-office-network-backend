import * as admin from 'firebase-admin';
import { MyError } from './my-error';
import { Id } from 'interfaces';

export class BaseService<T extends Id> {
    public baseCollection: Array<T>;
    constructor(
        public myErr?: MyError,
        public path?: string
    ) {}

    read(): Promise<Array<T>> {
        return new Promise((resolve, reject) => {
            return admin.firestore().collection(this.path).get();
        });
    }

    create(c: new (s) => T, id: string): T {
        return new c(id);
    }
}
