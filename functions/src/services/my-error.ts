import { throwError } from 'rxjs';
import * as functions from 'firebase-functions';

export class MyError {
	quotaErrMsg = 'Too many requests Firebase Request Quota exceeded. Please try again after 12 hours';

	constructor() {}

	public handleError<T>(error: Response | any) {
		let errMsg: string;
		if (error instanceof Response) {
			const body = error.json() || '';
			const err = JSON.stringify(body);
			errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
		} else {
			errMsg = error.message ? error.message : error.toString();
		}
		if (error === 'Quota exceeded.') {
			functions.logger.error(this.quotaErrMsg);
		}

		return throwError(errMsg);
	}
}
