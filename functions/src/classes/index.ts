export * from './Config.class';
/**
 * Create a Config.class.ts file contain something like
 * export class Config {
    public static FbSecret = {
            type: "service_account",
            project_id: "project-id",
            private_key_id: "privatekey",
            private_key: "key",
            client_email: "email"
            client_id: "client id",
            auth_uri: "authuri",
            token_uri: "token",
            auth_provider_x509_cert_url: "authprovider",
            client_x509_cert_url: "url"
        };
    }
 */